Param(
    [Parameter(Mandatory=$true)]
    [string]$path
)

if (!$path.Contains("slv-wpf01")){
    Write-output "Not a valid DFS path, valid path must start with \\slv-wpf01"
    exit -1
}

# Get all sub folders under the path
$lobFolders = Get-Childitem -Directory $path
foreach($lobFolder in $lobFolders){
    # Get all engagement folders, iterate and rename using the engagement number
    $engagementFolders = Get-Childitem -Directory $lobFolder.PSPath
    foreach($engagementFolder in $engagementFolders){
        [int]$firstHyphenPos =  $engagementFolder.Name.IndexOf("-");
        if ($firstHyphenPos -eq -1) {
            #Write-Output 'Corrected & Skipping'
            continue
        }
        Write-Output $engagementFolder.FullName
        [string]$engagementNumber = $engagementFolder.Name.Substring(0, $firstHyphenPos)
        try{
        Rename-Item -Path $engagementFolder.PSPath -NewName $engagementNumber
        } catch {
           Write-Output $Error[0].Exception
        }
    }
}