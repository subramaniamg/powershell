﻿<#
==================================================================================================== 
Name				:	hlhzsite.ps1
Developer			:   GS
Purpose				:   Command to start or stop a site on IIS
Ticket/Reference	:	STRY0048441
Arguments           
Name                Type        Required            Description
---------------------------------------------------------------------------------------------------
$stop               bool        No                  Take the site offline   
$start              bool        No                  Bring the site online
$siteRoot           string      Yes                 Absolute path to the folder HLHZNET\ASPDOTNET
$sitename           string      Yes                 Web application name

Usage
hlhzsite.ps1 -stop -siteroot D:\HLHZNET\ASPDOTNET -sitename WpfApp
hlhzsite.ps1 -start -siteroot D:\HLHZNET\ASPDOTNET -sitename WpfApp
=================================================================================================== 
#>
param (
    [switch] $stop,
    [switch] $start,
    [Parameter(Mandatory)]
    [string] $siteRoot,
    [Parameter(Mandatory)]
    [string] $siteName
)

[string]$scriptFolder = Split-Path $script:MyInvocation.MyCommand.Path
[string]$sourceDummyOfflineFilePath = Join-Path $scriptFolder "\app_offline1.htm"
[string]$sourceOfflineImagePath = Join-Path $scriptFolder "\maintenance.jpg"
[string]$sitePath = Join-Path -Path $siteRoot -ChildPath $siteName
[string]$siteOfflineFilePath = $sitePath + "\app_offline.htm"
[string]$siteDummyOfflineFilePath = $sitePath + "\app_offline1.htm"

if ($stop -eq $false -and $start -eq $false)
{
    Write-Output "Missing argument start or stop"
    exit 1
}

if ($start)
{
    # Rename the app_offline.htm file to app_offline1.htm only if the dummy file does not already exist
    if ([System.IO.File]::Exists($siteOfflineFilePath) -and ![System.IO.File]::Exists($siteDummyOfflineFilePath))
    {
        Rename-Item -Path $siteOfflineFilePath -NewName 'app_offline1.htm'
    }
    
    # Delete the app_offline.htm file if exists
    if ( [System.IO.File]::Exists($siteOfflineFilePath))
    {
        Remove-Item -Path $siteOfflineFilePath
    }
}
elseif ($stop)
{
    Copy-Item -Path $sourceOfflineImagePath -Destination $siteRoot

    if (![System.IO.File]::Exists($siteDummyOfflineFilePath))
    {
        Copy-Item -Path $sourceDummyOfflineFilePath  -Destination $sitePath
        Rename-Item -Path $siteDummyOfflineFilePath -NewName 'app_offline.htm'
    }
    else
    {
        Rename-Item -Path $siteDummyOfflineFilePath -NewName 'app_offline.htm'
    }
}